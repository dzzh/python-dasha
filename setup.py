# coding=utf-8

from distutils.core import setup

setup(
    name='python-dasha',
    description='A Python implementation of Dasha data mining algorithm for comparing transaction databases using association rules mining techniques',
    version='0.01',
    author='Źmicier Žaleźničenka',
    author_email='zmicier.zaleznicenka@gmail.com',
    url='https://bitbucket.org/dzzh/python-dasha',
    license='MIT License',
    platforms=['any'],
    zip_safe=True,
    packages=['dasha'],
    scripts=['scripts/python-dasha']
)