python-dasha
===============================================================================

This is a Python implementation of an experimental data mining algorithm called Dasha. The algorithm is used to find differences in patterns existing in transaction databases using association rule mining. It uses the FP-growth algorithm to find frequent itemsets in baseline and target datasets, builds the support trees from the retrieved itemsets and compares them starting from the root in breadth-first order level-wise. The most important differences are reported for further manual analysis. The properties of this algorithm have been described in [my Master's thesis](http://repository.tudelft.nl/view/ir/uuid%3A076ec1f6-d6c6-4266-afc5-dd71e24d65b2/). 

Installation
------------

You can install the module by running `python setup.py install`.

Usage
-----

Install the module and run `python-dasha -h` to see a list of available options.

A working example: `python-dasha -s 1000 -d 5 datasets/t101.dat datasets/t102.dat`

References
----------

The FP-growth algorithm used to find frequent itemsets is based on the [implementation](https://github.com/enaeseth/python-fp-growth) by Eric Naeseth.

License
-------

`python-dasha` is available under the terms of MIT License.

Copyright © 2013 [Źmicier Žaleźničenka][me]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

[me]: http://bitbucket.org/dzzh/