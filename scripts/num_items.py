"""
Calculate number of different words in a given file
"""

from optparse import OptionParser

if __name__ == '__main__':

    p = OptionParser(usage='%prog file')
    p.add_option('-s', '--separator', type='string', help='Words separator (default: \' \')')
    p.set_defaults(separator=' ')

    options, args = p.parse_args()
    if len(args) < 1:
        p.error('must provide path to the file for analysis')

    with open(args[0]) as data_file:
        words = set()
        for row in data_file.readlines():
            for word in row.split(options.separator):
                words.add(word.strip())
        print len(words)