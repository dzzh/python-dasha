"""
Calculate average length of a row in terms of a number of words for a given file
"""

from optparse import OptionParser

if __name__ == '__main__':

    p = OptionParser(usage='%prog file')
    p.add_option('-s', '--separator', type='string', help='Words separator (default: \' \')')
    p.set_defaults(separator=' ')

    options, args = p.parse_args()
    if len(args) < 1:
        p.error('must provide path to the file for analysis')

    with open(args[0]) as data_file:
        words = 0
        lines = 0.0
        for row in data_file.readlines():
            words += len(row.split(options.separator))
            lines += 1
        print words/lines if lines else 0