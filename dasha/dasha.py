# coding=utf-8
from collections import defaultdict
from itertools import imap
import logging
from operator import itemgetter
import math
import datetime

import os

from dasha_classes import *
from dasha_utils import *

__author__ = 'Źmicier Žaleźničenka <zmicier.zaleznicenka@gmail.com>'
__copyright__ = 'Copyright © 2013 Źmicier Žaleźničenka'
__license__ = 'MIT License'

logger = logging.getLogger(__name__)

#Threshold for comparing dangers between two rules
FLOAT_THRESHOLD = 0.01


def analyze_data(baseline_transactions, target_transactions, minsup, mindiff, quiet):
    """
    Main data analysis method. Build support trees for baseline and target transactions,
    compare the trees and output the most representative association rules.
    @param [] baseline_transactions: list of transactions in baseline dataset
    @param [] target_transactions: list of transactions in target dataset
    @param int minsup: minimum support in number of transactions
    @param int mindiff: minimum difference between itemset supports to consider violation important, in percents
    @param bool quiet: do not output resulting violations if true
    @return [ViolatingCounter]: list of violations
    """
    logger.info('Running test with minsup %d and mindiff %d%%' % (minsup, mindiff))

    #Update minimum support for target to better work with boundary situations where baseline
    #exceeds boundary a little, and target does not. If not decreasing target support,
    #for such cases target will be always reported as 0.
    scaling_factor = len(baseline_transactions) / float(len(target_transactions))
    target_minsup = int((minsup - minsup * mindiff / 100.0) / scaling_factor)
    if target_minsup == minsup and target_minsup > 1:
        target_minsup -= 1
    if target_minsup < 1:
        target_minsup = 1

    #Build FP-trees and support trees
    t2 = datetime.datetime.now()
    baseline_fp_tree = build_fp_tree(baseline_transactions)
    target_fp_tree = build_fp_tree(target_transactions)
    t3 = datetime.datetime.now()

    time_fp_gen = t3 - t2
    logger.info('Generated fp-trees in %s' % time_fp_gen)

    baseline_itemsets, baseline_support_tree = build_support_tree(baseline_fp_tree, minsup)

    t4 = datetime.datetime.now()
    logger.info('Found itemsets with support >=%d in baseline in %s' % (minsup, t4 - t3))

    used_memory_mb_1 = get_memory_usage_in_mb()
    if used_memory_mb_1 > -1:
        logger.info('Used memory: %d MB' % int(used_memory_mb_1))

    target_itemsets, target_support_tree = build_support_tree(target_fp_tree, target_minsup)

    used_memory_mb_2 = get_memory_usage_in_mb()
    if used_memory_mb_2 > -1:
        logger.info('Used memory: %d MB' % int(used_memory_mb_2))

    t5 = datetime.datetime.now()
    logger.info('Found itemsets with support >=%d in target in %s' % (target_minsup, t5 - t4))

    time_st_gen = t5 - t3
    logger.info('Support trees generated in %s' % time_st_gen)

    #Find association rules using support trees comparison algorithm
    rules = compare_itemset_trees(baseline_support_tree, baseline_fp_tree, target_support_tree, target_fp_tree, mindiff)

    used_memory_mb_3 = get_memory_usage_in_mb()
    if used_memory_mb_3 > -1:
        logger.info('Used memory: %d MB' % int(used_memory_mb_3))

    t6 = datetime.datetime.now()
    time_comp = t6 - t5
    logger.info('Compared trees in %s' % time_comp)
    logger.info('Unoptimized rules generated: %d' % len(rules))

    #Select the most representative rules from the list
    result = optimize_rules(baseline_fp_tree, target_fp_tree, rules)

    t7 = datetime.datetime.now()
    time_opt = t7 - t6
    logger.info('Optimized rules in %s' % time_opt)
    time_total = t7 - t2
    logger.info('Analysis completed in %s' % time_total)
    logger.info('Rules returned: %d' % len(result))

    #    Output the results
    if not quiet:
        for counter in result:
            logger.info(counter.str_important(minsup))

    logger.info('')
    logger.info('====================================')
    logger.info('   DASHA ALGORITHM SUMMARY REPORT  ')
    logger.info('Timings: %.1f\t%.1f\t%.1f\t%.1f\t%.1f' % (
        td2s(time_fp_gen), td2s(time_st_gen), td2s(time_comp), td2s(time_opt), td2s(time_total)))
    logger.info('Itemsets s/t: %d\t%d' % (baseline_itemsets, target_itemsets))
    logger.info('Supports s/t: %d\t%d' % (minsup, target_minsup))
    if os.name == 'posix':
        logger.info('Memory, mb: %d\t%d\t%d' % (used_memory_mb_1, used_memory_mb_2, used_memory_mb_3))
    logger.info('====================================')
    logger.info('')


def build_fp_tree(transactions):
    """
    Build an FP-tree out of source dataset. This FP-tree contains all items, appearing in the transaction
    not only frequent ones. This is needed as this property is used later for analysis.
    @param [] transactions: list of transactions
    """
    items = defaultdict(lambda: 0)

    # Load the passed-in transactions and count the support that individual items have.
    for transaction in transactions:
        for item in transaction:
            items[item] += 1

    def fp_sort_items():
        """
        Sort list of items with frequencies following FP-tree order (frequency-desc, natural-asc)
        @param [(item,frequency)]: list of items
        @return sorted list without frequencies
        """
        natural_order = sorted(items.iteritems(), key=itemgetter(0))
        full_sort = sorted(natural_order, key=itemgetter(1), reverse=True)
        return [item[0] for item in full_sort]

    sorted_items = fp_sort_items()

    def preprocess_transaction(transaction):
        """
        Prepare the transaction for addition to the tree: select only frequent items and sort
        them in frequency decreasing order.
        """
        transaction = filter(lambda v: v in items, transaction)
        transaction.sort(key=lambda v: sorted_items.index(v))
        return transaction

    tree = FPTree()

    for transaction in imap(preprocess_transaction, transactions):
        tree.add_transaction(transaction)
    return tree


def build_support_tree(fp_tree, minsup):
    """
    Build a support tree containing the frequent itemsets out of FP-tree.
    @param FPTree fp_tree: FP-tree generated from a source dataset
    @param int minsup: number of transactions to consider itemset frequent
    @return FPTree, SupportTree
    """
    inside_add_transaction = datetime.timedelta()
    support_tree = SupportTree(fp_tree.num_transactions, fp_tree.sorted_items())
    itemsets = 0
    for record, support in find_suffix_in_fp_tree(fp_tree, [], minsup):
        t1 = datetime.datetime.now()
        support_tree.add_transaction(record, support, False)
        t2 = datetime.datetime.now()
        inside_add_transaction += t2 - t1
        itemsets += 1
    logger.info('Generated %d itemsets' % itemsets)
    logger.info('Time to build support tree from existing itemsets: %s' % inside_add_transaction)
    return itemsets, support_tree


def compare_itemset_trees(baseline_support_tree, baseline_fp_tree,
                          target_support_tree, target_fp_tree, mindiff):
    """
    Compare support trees and return a list of important association rules grouped by antecedents.
    @param SupportTree baseline_support_tree: support tree of baseline itemsets
    @param FPTree baseline_fp_tree: FP-tree built from baseline dataset
    @param SupportTree target_support_tree: support tree of target itemsets
    @param FPTree target_fp_tree: FP-tree built from target dataset
    @param int mindiff: minimum difference between supports in percents to consider violation important
    @return [AssociationRule]: list of violating association rules grouped by antecedents
    """
    scaling_factor = baseline_support_tree.root.support / float(target_support_tree.root.support)
    rules_calls = [0]
    rule_calls = [0]
    time_in_rules = [datetime.timedelta()]
    time_in_rule_support = [datetime.timedelta()]
    time_in_rule_search = [datetime.timedelta()]

    def store_rule(antecedent, consequent):
        """
        Store association rule in a list.
        @param [] antecedent: an itemset representing rule antecedent
        @param consequent: an item that is a consequent in a rule
        """
        t1 = datetime.datetime.now()
        rule_calls[0] += 1
        #Calculate support for antecedents and consequents for both baseline and target

        if not antecedent:
            baseline_antecedent_support = baseline_fp_tree.num_transactions
        else:
            baseline_antecedent_node = baseline_support_tree.get_node_for_pattern(antecedent)
            baseline_antecedent_support = baseline_antecedent_node.support if baseline_antecedent_node \
                else baseline_fp_tree.get_itemset_support(antecedent)

        baseline_consequent_support = baseline_support_tree.get_node_for_pattern(antecedent + [consequent]).support

        if not antecedent:
            target_antecedent_support = target_fp_tree.num_transactions
        else:
            target_antecedent_node = target_support_tree.get_node_for_pattern(antecedent)
            target_antecedent_support = target_antecedent_node.support if target_antecedent_node \
                else target_fp_tree.get_itemset_support(antecedent)

        target_node = target_support_tree.get_node_for_pattern(antecedent + [consequent])
        target_consequent_support = target_node.support if target_node else 0

        #No need to store rules with dissimilar antecedents - they are stored with similar antecedents
        if not are_itemset_supports_close(baseline_antecedent_support, target_antecedent_support * scaling_factor,
                                          mindiff):
            return

        t2 = datetime.datetime.now()
        time_in_rule_support[0] += t2 - t1

        rule = None
        key = tuple(antecedent)
        if key in rules:
            rule = rules[key]
        t3 = datetime.datetime.now()
        time_in_rule_search[0] += t3 - t2
        if not rule:
            rule = AssociationRule(antecedent, baseline_antecedent_support, target_antecedent_support,
                                   baseline_fp_tree.num_transactions, target_fp_tree.num_transactions, scaling_factor)

        new_consequent = Consequent(consequent, baseline_consequent_support, target_consequent_support)

        rule.add_consequent(new_consequent)
        rules[key] = rule

    def store_rules(baseline_node):
        """
        Generate and store association rules for a violation found between baseline and target trees.
        @param SupportNode baseline_node: node in baseline tree that hasn't corresponding non-violating node in target.
        """
        rules_calls[0] += 1
        t1 = datetime.datetime.now()
        pattern = baseline_node.pattern()
        target_node = target_support_tree.get_node_for_pattern(pattern)
        target_support = target_node.support if target_node else 0

        if not target_support:
            #There is no corresponding node in the target tree, thus only one rule has to be generated
            antecedent = list(pattern)
            consequent = antecedent.pop()
            store_rule(antecedent, consequent)
        else:
            #Target tree has a corresponding node, all possible rules with 1-node consequent have to be checked
            for i in range(len(pattern)):
                antecedent = list(pattern)
                consequent = antecedent.pop(i)
                store_rule(antecedent, consequent)
        t2 = datetime.datetime.now()
        time_in_rules[0] += t2 - t1

    queue = []
    [queue.append(child) for _, child in baseline_support_tree.root.children]
    rules = {}

    calls = 0
    while queue:
        calls += 1
        current_node = queue.pop(0)
        pattern = current_node.pattern()
        baseline_support = current_node.support
        target_node = target_support_tree.get_node_for_pattern(pattern)

        if target_node:
            target_support = target_node.support * scaling_factor
            if are_itemset_supports_close(baseline_support, target_support, mindiff):
                [queue.append(child) for _, child in current_node.children]
                logger.debug('+++ %s (%f, %f)' % (pattern, baseline_support, target_support))
            else:
                logger.debug('--- %s: confidence violation - (%f, %f)' % (pattern, baseline_support, target_support))
                store_rules(current_node)
        else:
            logger.debug('--- %s (%f)' % (pattern, baseline_support))
            store_rules(current_node)

    logger.info('Rules calls: %d' % rules_calls[0])
    logger.info('Rule calls: %d' % rule_calls[0])
    logger.info('Time in store_rules: %s' % time_in_rules[0])
    logger.info('Time in store_rule to calculate supports: %s' % time_in_rule_support[0])
    logger.info('Time in store_rule to search in rules: %s' % time_in_rule_search[0])
    return rules.values()


def optimize_rules(baseline_fp_tree, target_fp_tree, rules):
    """
    Remove duplicate rules from the diff and add categories distribution for the remaining ones
    @param FPTree baseline_fp_tree: fp-tree built from baseline dataset
    @param FPTree target_fp_tree: fp-tree built from target dataset
    @param [AssociationRule] rules: list of association rules
    @return [ViolatingCounter]: list of the most representative association rules
    """
    scaling_factor = baseline_fp_tree.num_transactions / float(target_fp_tree.num_transactions)
    result = []
    for index in get_violating_indices(rules):
        pattern = get_violating_rule_for_index(rules, index).antecedent
        baseline_antecedent_support, baseline_consequent_supports = get_categories_distribution(baseline_fp_tree,
                                                                                                pattern, index)
        target_antecedent_support, target_consequent_supports = get_categories_distribution(target_fp_tree, pattern,
                                                                                            index)
        counter = ViolatingCounter(index, pattern, baseline_antecedent_support, target_antecedent_support,
                                   baseline_fp_tree.num_transactions, target_fp_tree.num_transactions, scaling_factor)
        items = set()
        for i in baseline_consequent_supports:
            items.add(i)
        for i in target_consequent_supports:
            items.add(i)
        for i in items:
            baseline_support = baseline_consequent_supports[i] if i in baseline_consequent_supports else 0
            target_support = target_consequent_supports[i] if i in target_consequent_supports else 0
            entry = Consequent(i, baseline_support, target_support)
            counter.add_consequent(entry)
        result.append(counter)
    return result


def find_suffix_in_fp_tree(fp_tree, suffix, minsup):
    """
    Find suffix in conditional tree
    @param fp_tree: tree
    @param suffix: suffix
    """
    for item, nodes in fp_tree.items():
        support = sum(n.count for n in nodes)
        if support >= minsup and item not in suffix:
            found_set = [item] + suffix
            yield found_set, support

            # Build a conditional tree and recursively search for frequent itemsets within it.
            cond_tree = conditional_tree_from_paths(fp_tree.prefix_paths(item))
            for s in find_suffix_in_fp_tree(cond_tree, found_set, minsup):
                yield s  # pass along the good news to our caller


def conditional_tree_from_paths(paths):
    """Build a conditional FP-tree from the given prefix paths."""
    tree = FPTree()
    condition_item = None
    items = set()

    # Import the nodes in the paths into the new tree. Only the counts of the
    # leaf notes matter; the remaining counts will be reconstructed from the
    # leaf counts.
    for path in paths:
        if condition_item is None:
            condition_item = path[-1].item

        point = tree.root
        for node in path:
            next_point = point.search(node.item)
            if not next_point:
                # Add a new node to the tree.
                items.add(node.item)
                count = node.count if node.item == condition_item else 0
                next_point = FPNode(tree, node.item, count)
                point.add_child(next_point)
                tree._update_route(next_point)
            point = next_point

    assert condition_item is not None

    # Calculate the counts of the non-leaf nodes.
    for path in tree.prefix_paths(condition_item):
        count = path[-1].count
        for node in reversed(path[:-1]):
            node._count += count

    return tree


def get_violating_indices(rules):
    """
    Return a merged list of indices for all the nodes that differ between the datasets.
    @param [AssociationRule] rules: result of analyzing the itemsets
    @return [] children: indices of children that differ somewhere between the itemsets
    """
    children = set()
    for node in rules:
        for child in node.consequents:
            children.add(get_item_index(child.item))
    return list(children)


def get_violating_rule_for_index(rules, index):
    """
    Take an index of a violating counter and find the most important rule for it to report.
    Violation is chosen by danger, then by support.
    @param [ViolatingCounter] rules: difference between the datasets
    @param index: index of a violating counter
    @return Consequent: most important violation
    """
    selected_rule = None
    selected_danger = None
    for rule in rules:
        if rule.has_consequent_with_index(index):
            if not selected_rule:
                selected_rule = rule
                selected_danger = rule.danger(index)
            else:
                current_danger = rule.danger(index)
                if current_danger > selected_danger and current_danger - selected_danger >= FLOAT_THRESHOLD:
                    selected_rule = rule
                    selected_danger = current_danger
                #If dangers are almost equal, take baseline support as tie breaker
                elif math.fabs(current_danger - selected_danger) < FLOAT_THRESHOLD \
                    and rule.baseline_support > selected_rule.baseline_support \
                        and rule.baseline_support - selected_rule.baseline_support >= FLOAT_THRESHOLD:
                    selected_rule = rule
                    selected_danger = current_danger
    return selected_rule


def get_categories_distribution(fp_tree, pattern, index):
    """
    Take counter index, find all its categories and return distribution of their supports.
    Use fp tree to make calculations.
    @param FPTree fp_tree: Instance of FP-tree storing representing the interesting dataset.
    @param [] pattern: pattern that have to be presented in all transaction we use for calculations
    @param index: index of a violating counter
    """
    pattern_support = fp_tree.get_itemset_support(pattern)
    categories = fp_tree.categories(get_item_index, index)
    support_dict = {}
    for category in categories:
        itemset = fp_tree.sort_transaction(pattern + [category])
        support_dict[category] = fp_tree.get_itemset_support(itemset)
    return pattern_support, support_dict


def is_itemset_in_transaction(transaction, itemset):
    """
    Scan the transaction and report whether a given itemset is contained in it.
    @param [] transaction: transaction to scan
    @param [] itemset: itemset to look for
    @return True if itemset is found, False otherwise
    """
    trans_set = set(transaction)
    item_set = set(itemset)
    return item_set.issubset(trans_set)


def get_support_for_pattern(transactions, pattern):
    """
    Get support of a pattern in source dataset using brute-force.
    @param [] transactions: source dataset
    @param pattern: pattern to look for
    @return int support
    """
    support = 0
    for transaction in transactions:
        if is_itemset_in_transaction(transaction, pattern):
            support += 1
    return support


def run(source, target, options):
    baseline_transactions = file_to_transactions(source, options.separator, options.numeric)
    target_transactions = file_to_transactions(target, options.separator, options.numeric)

    analyze_data(baseline_transactions, target_transactions, options.minsup, options.mindiff, options.quiet)
