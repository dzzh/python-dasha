# coding=utf-8
from operator import itemgetter
from collections import namedtuple
import math
from dasha_utils import get_item_index

__author__ = 'Źmicier Žaleźničenka <zmicier.zaleznicenka@gmail.com>'
__copyright__ = 'Copyright © 2013 Źmicier Žaleźničenka'
__license__ = 'MIT License'

#FP-tree implementation is courtesy of Eric Naeseth (https://github.com/enaeseth/python-fp-growth)


class FPTree(object):
    Route = namedtuple('Route', 'head tail')

    def __init__(self):
        # The root node of the tree.
        self._root = FPNode(self, None, None)

        # A dictionary mapping items to the head and tail of a path of
        # "neighbors" that will hit every node containing that item.
        self._routes = {}

        #Number of transactions stored in the tree.
        self._num_transactions = 0

        #Cache of the list of FP-ordered items
        self._items_cache = []

    @property
    def root(self):
        return self._root

    @property
    def num_transactions(self):
        return self._num_transactions

    def add_transaction(self, transaction):
        self._num_transactions += 1
        current_node = self._root
        self._items_cache = []  # tree changes, clear the cache to recalculate it later
        for item in transaction:
            next_node = current_node.search(item)
            if next_node:
                # There is already a node in this tree for the current transaction item; reuse it.
                next_node.increment_count()
            else:
                # Create a new current_node and add it as a child of the current_node we're currently looking at.
                next_node = FPNode(self, item)
                current_node.add_child(next_node)

                # Update the route of nodes that contain this item to include our new node.
                self._update_route(next_node)

            current_node = next_node

    def _update_route(self, node):
        """Add the given node to the route through all nodes for its item."""
        assert self is node.tree

        try:
            route = self._routes[node.item]
            route[1].neighbor = node  # route[1] is the tail
            self._routes[node.item] = self.Route(route[0], node)
        except KeyError:
            # First node for this item; start a new route.
            self._routes[node.item] = self.Route(node, node)

    def items(self):
        """
        Generate one 2-tuples for each item represented in the tree. The first
        element of the tuple is the item itself, and the second element is a
        generator that will yield the nodes in the tree that belong to the item.
        """
        for item in self._routes.iterkeys():
            yield (item, self.nodes(item))

    def frequencies(self):
        """
        Return dictionary with item frequencies.
        """
        result = {}
        for item, nodes in self.items():
            result[item] = sum([node.count for node in nodes])
        return result

    def sorted_items(self):
        """
        Return list item sorted in desc FP-tree order (first desc frequency, then asc natural).
        The result is sorted first by frequency in descending order, then by item in ascending order.
        After the first calculation the items are stored in cache and reused later if the tree hasn't changed.
        """
        if not self._items_cache:
            natural_order = sorted(self.frequencies().iteritems(), key=itemgetter(0))
            full_sort = sorted(natural_order, key=itemgetter(1), reverse=True)
            self._items_cache = [item[0] for item in full_sort]
        return self._items_cache

    def get_itemset_support(self, itemset):
        """
        Return support of a given itemset.
        @param [] itemset: itemset to use
        """
        if not itemset:
            return self.num_transactions

        sorted_itemset = self.sort_transaction(itemset)

        last_item = sorted_itemset[-1]
        paths = self.prefix_paths(last_item)
        support = 0
        for path in paths:
            itemset_copy = list(sorted_itemset)
            path_support = path[-1].count
            for node in path:
                if node.item in itemset_copy:
                    itemset_copy.remove(node.item)
            if not itemset_copy:
                support += path_support
        return support

    def sort_transaction(self, transaction):
        return sorted(transaction, key=lambda x: self.sorted_items().index(x))

    def nodes(self, item):
        """
        Generate the sequence of nodes that contain the given item.
        """
        try:
            node = self._routes[item][0]
        except KeyError:
            return

        while node:
            yield node
            node = node.neighbor

    def prefix_paths(self, item):
        """Generate the prefix paths that end with the given item."""

        def collect_path(node):
            path = []
            while node and not node.root:
                path.append(node)
                node = node.parent
            path.reverse()
            return path

        return (collect_path(node) for node in self.nodes(item))

    def categories(self, index_function, index):
        """Get all categories existing in the tree for a given index."""
        for item in self.items():
            category = item[0]
            if index_function(category) == index:
                yield category

    def inspect(self):
        print 'Tree:'
        self.root.inspect(1)

        print
        print 'Routes:'
        for item, nodes in self.items():
            print '  %r' % item
            for node in nodes:
                print '    %r' % node


class FPNode(object):
    """A node in an FP-tree."""

    def __init__(self, tree, item, count=1):
        self._tree = tree
        self._item = item
        self._count = count
        self._parent = None
        self._children = {}
        self._neighbor = None

    def add_child(self, child):
        """Add the given FPNode as a child of this node.
        @param FPNode child: child to add
        """
        if not child.item in self._children:
            self._children[child.item] = child
            child.parent = self

    def search(self, item):
        """If a current node has a child with a given item, return it. Return None otherwise."""
        if item in self._children:
            return self._children[item]
        else:
            return None

    def __contains__(self, item):
        return item in self._children

    @property
    def tree(self):
        """The tree in which this node appears."""
        return self._tree

    @property
    def item(self):
        """The item contained in this node."""
        return self._item

    @property
    def count(self):
        """The count associated with this node's item."""
        return self._count

    def increment_count(self):
        """Increments the count associated with this node's item."""
        if self._count is None:
            raise ValueError('Root nodes have no associated count.')
        self._count += 1

    @property
    def root(self):
        """True if this node is the root of a tree; false if otherwise."""
        return self._item is None and self._count is None

    @property
    def leaf(self):
        """True if this node is a leaf in the tree; false if otherwise."""
        return len(self._children) == 0

    @property
    def parent(self):
        """The node's parent."""
        return self._parent

    @parent.setter
    def parent(self, value):
        if value and value.tree is not self.tree:
            raise ValueError("Cannot have a parent from another tree.")
        self._parent = value

    @property
    def neighbor(self):
        """The node's neighbor; the one with the same value that is "to the right of it in the tree."""
        return self._neighbor

    @neighbor.setter
    def neighbor(self, value):
        if value is not None and not isinstance(value, FPNode):
            raise TypeError("A node must have an FPNode as a neighbor.")
        if value and value.tree is not self.tree:
            raise ValueError("Cannot have a neighbor from another tree.")
        self._neighbor = value

    @property
    def children(self):
        """The nodes that are children of this node."""
        return tuple(self._children.itervalues())

    def inspect(self, depth=0):
        print ('  ' * depth) + repr(self)
        for child in self.children:
            child.inspect(depth + 1)

    def __repr__(self):
        if self.root:
            return "<%s (root)>" % type(self).__name__
        return "<%s %r (%r)>" % (type(self).__name__, self.item, self.count)


class SupportTree(object):
    def __init__(self, root_support, sorted_items):
        self._root = SupportNode(None, root_support)
        self._sorted_items = sorted_items

    @property
    def root(self):
        return self._root

    def add_transaction(self, transaction, support=0, increment=True):
        """
        Add the items contained in the transaction to the tree and update their supports.
        """
        sorted_transaction = self.sort_transaction(transaction)
        current_node = self._root
        for item in sorted_transaction:
            next_node = current_node.search(item)
            if next_node:
                #There already is a node in this place. Increase its support.
                if increment:
                    next_node.increment_count()
            else:
                #Node not found, insert it into the tree.
                next_node = SupportNode(item)
                current_node.add_child(next_node)
                if support:
                    next_node.support = support
            current_node = next_node
        if support:
            current_node.support = support

    def sort_transaction(self, transaction):
        """
        Sort items in the transaction following the FP-tree order: first desc by frequency, then asc in natural order.
        @param [] transaction: transaction to sort
        @return sorted transaction
        """
        return sorted(transaction, key=lambda x: self._sorted_items.index(x))

    def get_node_for_pattern(self, pattern, root=None):
        """
        Return last node in path for exact path in tree starting from given node or root.
        For this method, the looked path should exist as is, and only its support is returned.
        I.e. if the pattern is 'ac', only support for 'c' in path 'root->a->c' will be returned.
        Path 'root->a->b->c' won't be considered in this situation.
        Is intended to be used for fast retrieval of paths support from itemsets tree. If using
        with transaction tree, will give wrong values.
        @param [] pattern: pattern to look for
        @param SupportNode or None root: node to start search. If not set, root is used.
        """
        try:
            sorted_pattern = self.sort_transaction(pattern)
        except ValueError:
            #If any of the nodes we look for is not in the tree, we immediately return None
            return None

        if not root:
            current_node = self.root
        else:
            current_node = root

        if not pattern:
            return root

        for child in [c[1] for c in current_node.children]:
            if child.item == sorted_pattern[0]:
                if len(pattern) > 1:
                    return self.get_node_for_pattern(sorted_pattern[1:], child)
                else:
                    return child
        return None

    def inspect(self):
        print 'Tree:'
        self.root.inspect(0)


class SupportNode(object):
    def __init__(self, item, support=1):
        self._item = item
        self._support = support
        self._parent = None
        self._children = {}

    def add_child(self, child):
        """
        Add a given node as a child of a current node
        @param SupportNode child: child to add
        """
        if not child.item in self._children:
            self._children[child.item] = child
            child.parent = self

    def search(self, item):
        """If a current node has a child with a given item, return it. Return None otherwise."""
        if item in self._children:
            return self._children[item]
        else:
            return None

    def pattern(self):
        """Return list of items following the path in the tree starting from root down to and including the item"""
        if self.root:
            return None
        elif self.parent.root:
            return [self.item]
        else:
            return self.parent.pattern() + [self.item]

    @property
    def item(self):
        """The item contained in this node"""
        return self._item

    @property
    def support(self):
        """Tree support for the prefix ending at this node"""
        return self._support

    @support.setter
    def support(self, value):
        self._support = value

    def increment_support(self):
        """Increment node support by 1"""
        if self._support is None:
            raise ValueError('Root nodes do not have support')
        self._support += 1

    @property
    def root(self):
        """True if the node is root, false otherwise"""
        return self._item is None

    @property
    def leaf(self):
        """True if the node is leaf, false otherwise"""
        return len(self._children) == 0

    @property
    def parent(self):
        """Parent node"""
        return self._parent

    @parent.setter
    def parent(self, value):
        self._parent = value

    @property
    def children(self):
        """Return children of this node"""
        return tuple(self._children.iteritems())

    def inspect(self, depth=0):
        if depth > 0:
            print ('|  ' * (depth - 1)) + '|--' + repr(self)
        else:
            print repr(self)
        for _, child in sorted(self.children, key=lambda i: i[0]):
            child.inspect(depth + 1)

    def __repr__(self):
        if self.root:
            return "<%s (root)>" % type(self).__name__
        return "%r (%r)" % (self.item, self.support)


class AssociationRule:
    #When calculating rule danger, support is considered less important than severity by this value
    SUPPORT_COEFFICIENT = 5

    def __init__(self, antecedent_pattern, baseline_support, target_support,
                 num_baseline_transactions, num_target_transactions, scaling_factor=1):
        self._antecedent = antecedent_pattern
        self._baseline_support = baseline_support
        self._target_support = target_support
        self._scaling_factor = scaling_factor
        self._children = []
        self._num_baseline_transactions = num_baseline_transactions
        self._num_target_transactions = num_target_transactions

    @property
    def num_baseline_transactions(self):
        return self._num_baseline_transactions

    @property
    def num_target_transactions(self):
        return self._num_target_transactions

    @property
    def antecedent(self):
        return self._antecedent

    @property
    def baseline_support(self):
        return self._baseline_support

    @property
    def target_support(self):
        return self._target_support * self._scaling_factor

    @property
    def target_support_unscaled(self):
        return self._target_support

    @property
    def baseline_support_percents(self):
        return int(float(self.baseline_support) / self.num_baseline_transactions * 100)

    @property
    def target_support_percents(self):
        return int(float(self.target_support_unscaled) / self.num_target_transactions * 100)

    @property
    def scaling_factor(self):
        return self._scaling_factor

    @property
    def consequents(self):
        """
        @return [Consequent] children
        """
        return self._children

    def add_consequent(self, child):
        self.consequents.append(child)
        child.parent = self

    def severity(self, index=0):
        """
        Severity metric is used to find how severe the violation flagged in a rule is.
        Calculates difference between consequent supports in baseline and target to find result.
        @param int index: counter index, if not set then all consequents are considered
        """
        diff = 0
        total = 0
        for c in self.consequents:
            if not index or get_item_index(c.item) == index:
                diff += math.fabs(c.baseline_support - c.target_support)
                total += c.baseline_support + c.target_support
        return float(diff) / total

    def danger(self, index=0):
        """
        Return danger, a cumulative metric consisting of severity and support.
        It is used to select most important rule for given index and sort items for presenting.
        @param int index: counter index, if not set then all consequents are considered
        """
        return self.severity(index) + float(self.baseline_support_percents) / 100 / self.SUPPORT_COEFFICIENT

    def has_consequent_with_index(self, index):
        for c in self.consequents:
            if get_item_index(c.item) == index:
                return True
        return False

    def __str__(self):
        if not self.scaling_factor == 1:
            result = 'Antecedent: %s (supports %.2f, %.2f (%.2f)) \n' % (
                self.antecedent, self.baseline_support, self.target_support, self.target_support_unscaled)
        else:
            result = 'Antecedent: %s (supports %d, %d) \n' % (
                self.antecedent, self.baseline_support, self.target_support)
        for child in sorted(self.consequents, key=lambda i: i.item):
            result += child.__str__()
        return result

    def str_important(self, minsup):
        result = 'Antecedent: %s (supports %d, %d (%d)) \n' % (
            self.antecedent, self.baseline_support, self.target_support, self.target_support_unscaled)
        for child in sorted(self.consequents, key=lambda i: i.item):
            if child.baseline_support >= minsup or child.target_support >= minsup:
                result += child.__str__()
        return result


class ViolatingCounter(AssociationRule):
    def __init__(self, counter_index, antecedent_pattern, baseline_support, target_support,
                 num_baseline_transactions, num_target_transactions, scaling_factor=1):
        AssociationRule.__init__(self, antecedent_pattern, baseline_support, target_support,
                                 num_baseline_transactions, num_target_transactions, scaling_factor)
        self.counter_index = counter_index


class Consequent():
    _parent = None

    def __init__(self, item, baseline_support, target_support):
        """
        @param item: differentiating item
        @param float baseline_support: itemset support in a baseline dataset
        @param float target_support: itemset support in a target dataset
        """
        self._item = item
        self._baseline_support = baseline_support
        self._target_support = target_support

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, parent):
        """
        @param AssociationRule parent: association rule to which the consequent belongs
        """
        self._parent = parent

    @property
    def item(self):
        return self._item

    @property
    def baseline_support(self):
        return self._baseline_support

    @property
    def target_support(self):
        return self._target_support * self.parent.scaling_factor

    @property
    def target_support_unscaled(self):
        return self._target_support

    @property
    def baseline_confidence(self):
        parent_support = float(self.parent.baseline_support) if self.parent else 1.0
        if not parent_support:
            return 0
        return self.baseline_support / parent_support

    @property
    def target_confidence(self):
        parent_support = float(self.parent.target_support) if self.parent else 1.0
        if not parent_support:
            return 0
        return self.target_support / parent_support

    def __str__(self):
        if not self.parent.scaling_factor == 1:
            return ' consequent %s with supports (%.2f, %.2f(%.2f)) and confidences (%.2f, %.2f)\n' \
                   % (self.item, self.baseline_support, self.target_support, self.target_support_unscaled,
                      self.baseline_confidence, self.target_confidence)
        else:
            return ' consequent %s with supports (%d, %d) and confidences (%.2f, %.2f)\n' \
                   % (self.item, self.baseline_support, self.target_support,
                      self.baseline_confidence, self.target_confidence)
