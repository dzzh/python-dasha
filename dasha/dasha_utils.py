import math
import os
import resource


def get_item_index(item):
    """
    Retrieve item index from its hashed representation.
    @param item: an item to find its index
    @return index
    """
    if type(item) == str:
        return item
    return item / 10


def td2s(timedelta):
    """
    Convert time interval to seconds with 1/10th precision to show in a summary report
    @param timedelta timedelta:
    @return float time in seconds
    """
    return timedelta.seconds + timedelta.microseconds/float(10**6)


def file_to_transactions(file_path, separator, is_numeric):
    """
    Read file with data to analyze and convert it to a list of transactions.
    @param str file_path: path to file with data
    @param str separator: words separator
    @param bool is_numeric: convert words to ints if True, otherwise treat as strings
    @return [] transactions
    """
    transactions = []
    with open(file_path) as data:
        for row in data.readlines():
            split = row.split(separator)
            transaction = []
            for word in split:
                word = word.strip()
                if is_numeric:
                    transaction.append(int(word))
                else:
                    transaction.append(word)
            transactions.append(transaction)
    return transactions


def are_itemset_supports_close(baseline_support, target_support, mindiff):
    """
    Return True if supports differ by less than mindiff, False otherwise.
    @param float baseline_support: support of baseline itemset
    @param float target_support: support of float itemset
    @param float mindiff: mindiff
    """
    sup_diff = math.fabs(baseline_support-target_support)/float(baseline_support)*100
    if sup_diff > mindiff:
        return False
    return True


def get_memory_usage_in_mb():
    """
    For POSIX operating systems, get amount of memory used by the process
    @return: allocated memory size in megabytes
    """
    if os.name == 'posix':
        return int(resource.getrusage(resource.RUSAGE_SELF).ru_idrss / 1024)
    return -1