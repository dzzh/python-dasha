#!/bin/bash

USAGE="Usage: $0 [10|40] [minsup|mindiff]"

if [ $# -ne 2 ]; then echo ${USAGE}; exit 1; fi

case $1 in
10)
    f1="t101.dat"
    f2="t102.dat"
    ;;
40)
    f1="t401.dat"
    f2="t402.dat"
    ;;
*)
    echo ${USAGE}
    exit 1
    ;;
esac

case $2 in
"minsup")
    echo "Running performance test with varying minsup on a dataset with average rule length $1"

    for i in 1000 500 400 300 250 200 150 100 50 25 10 5
    do
        python-dasha -s ${i} -d 5 -q datasets/${f1} datasets/${f2}
    done
    ;;

"mindiff")
    echo "Running performance test with varying mindiff on a dataset with average rule length $1"
    for i in 50 25 10
    do
        for j in 50 40 30 20 10 5
        do
            python-dasha -s ${i} -d ${j} -q datasets/${f1} datasets/${f2}
        done
    done
    ;;

*)
    echo ${USAGE}
    ;;
esac
