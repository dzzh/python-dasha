#!/bin/bash

echo "Running performance test for Mushroom datasets"

for i in 812 731 650 569 487 406 324 243
do
    python-dasha -s ${i} -d 5 -w ' ' -q datasets/mushroom1.dat datasets/mushroom2.dat
done